**Android Studio Project CipherTrack**


## Download

$ git clone https://bitbucket.org/bhumikkewlani/ciphertrack/

## Installation

Click "Sync Project with Gradle Files" and "Rebuild Project" at Android Studio Menu.

Run CipherTrack by Android Studio Menu > Run > Run CipherTrack.

## License

CipherTrack is available under the apache 2.0 license. See the LICENSE file for more info.

